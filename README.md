# Identity and Access management service

Identity and Access management service for Zebrunner

# Checking out and building

To check out the project and build from source, do the following:

    git clone git://github.com/zebrunner/iam-service.git
    cd iam-service
    ./gradlew build

DROP TABLE IF EXISTS USER_PREFERENCES;
CREATE TABLE IF NOT EXISTS USER_PREFERENCES (
  ID SERIAL,
  NAME VARCHAR(255) NOT NULL,
  VALUE VARCHAR(255) NOT NULL,
  USER_ID INT,
  MODIFIED_AT TIMESTAMP NOT NULL,
  CREATED_AT TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (ID),
  FOREIGN KEY (USER_ID) REFERENCES USERS (ID)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);
CREATE UNIQUE INDEX NAME_USER_ID_UNIQUE ON USER_PREFERENCES (NAME, USER_ID);
CREATE TRIGGER UPDATE_TIMESTAMP_USER_PREFERENCES
  BEFORE INSERT OR UPDATE
  ON USER_PREFERENCES
  FOR EACH ROW
EXECUTE PROCEDURE UPDATE_MODIFIED_AT();

INSERT INTO USER_PREFERENCES (NAME, VALUE, USER_ID) VALUES ('REFRESH_INTERVAL', '0', (SELECT ID FROM USERS WHERE USERNAME = 'anonymous')) ON CONFLICT DO NOTHING;
INSERT INTO USER_PREFERENCES (NAME, VALUE, USER_ID) VALUES ('DEFAULT_DASHBOARD', 'General', (SELECT ID FROM USERS WHERE USERNAME = 'anonymous')) ON CONFLICT DO NOTHING;
INSERT INTO USER_PREFERENCES (NAME, VALUE, USER_ID) VALUES ('THEME', '32', (SELECT ID FROM USERS WHERE USERNAME = 'anonymous')) ON CONFLICT DO NOTHING;
INSERT INTO USER_PREFERENCES (NAME, VALUE, USER_ID) VALUES ('DEFAULT_TEST_VIEW', 'runs', (SELECT ID FROM USERS WHERE USERNAME = 'anonymous')) ON CONFLICT DO NOTHING;

-- it is better to insert an obsolete DEFAULT_DASHBOARD preference for anonymous user
-- and remove it immediately in order to avoid potential collisions caused be db migration tool
DELETE FROM USER_PREFERENCES WHERE NAME = 'DEFAULT_DASHBOARD' AND USER_ID = (SELECT ID FROM USERS WHERE USERNAME = 'anonymous');

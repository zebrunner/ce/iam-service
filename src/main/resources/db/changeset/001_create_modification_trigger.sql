CREATE OR REPLACE FUNCTION update_modified_at()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
  new.modified_at := current_timestamp;
  RETURN new;
END;
$function$
;

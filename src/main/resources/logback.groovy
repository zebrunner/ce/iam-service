appender("Console-Appender", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} %level [%t] %logger{36}: %msg%n"
    }
}

root(INFO, ["Console-Appender"])
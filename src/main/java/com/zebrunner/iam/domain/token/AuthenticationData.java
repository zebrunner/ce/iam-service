package com.zebrunner.iam.domain.token;

import com.zebrunner.iam.domain.entity.UserSource;
import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.Set;

@Value
@Builder
public class AuthenticationData {

    Integer userId;
    UserSource userSource;
    Instant previousLogin;
    Set<String> permissionsSuperset;

    String authTokenType;
    String authToken;
    int authTokenExpirationInSecs;

    String refreshToken;

}

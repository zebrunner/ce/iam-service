package com.zebrunner.iam.domain.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class SearchCriteria {

    private final String query;
    private final String sortBy;
    private final SortOrder sortOrder;
    private final int page;
    private final int pageSize;
    private boolean isPublic;

    public SortOrder getSortOrder() {
        return sortBy != null && sortOrder == null ? SortOrder.ASC : sortOrder;
    }

}

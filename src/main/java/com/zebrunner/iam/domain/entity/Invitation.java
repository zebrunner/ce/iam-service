package com.zebrunner.iam.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@NamedEntityGraph(name = "invitation-with-invitor-and-group", attributeNodes = {
        @NamedAttributeNode("invitor"),
        @NamedAttributeNode("group")
})
@Table(name = "invitations")
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String email;
    private String token;

    @ManyToOne
    @JoinColumn(name = "invitor_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private User invitor;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "group_id", insertable = false, updatable = false)
    private Group group;

    @Column(name = "group_id")
    private Integer groupId;

    @Enumerated(EnumType.STRING)
    private UserSource source;

    @Generated(GenerationTime.ALWAYS)
    @Column(name = "created_at", insertable = false, updatable = false)
    private Instant createdAt;

    @Generated(GenerationTime.ALWAYS)
    @Column(name = "modified_at", insertable = false, updatable = false)
    private Instant modifiedAt;

}

package com.zebrunner.iam.domain.entity;

import java.util.Arrays;

public enum UserSource {

    INTERNAL,
    // TODO: 3/18/21 dkazak Remove LDAP next release
    LDAP;

    public static UserSource fromString(String value) {
        return Arrays.stream(UserSource.values())
                     .filter(status -> status.name().equals(value))
                     .findFirst()
                     .orElse(null);
    }

}

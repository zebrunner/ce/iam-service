package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Permission;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PermissionService {

    String WILDCARD_SUFFIX = "*";

    List<Permission> getAll();

    Set<Permission> getConciseSetByNames(Collection<String> names);

    Set<String> getSuperset(Collection<Group> groups, Collection<Permission> permissions);

}

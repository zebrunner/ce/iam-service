package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.UserSearchCriteria;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface UserService {

    String ANONYMOUS = "anonymous";

    SearchResult<User> search(UserSearchCriteria searchCriteria);

    Optional<User> getById(Integer id);

    List<User> getByIds(List<Integer> ids);

    Optional<User> getWithPermissionsById(Integer id);

    Optional<User> getWithPermissionsByUsername(String username);

    Optional<User> getByUsernameOrEmail(String usernameOrEmail);

    Optional<User> getByResetToken(String resetToken);

    User create(User user);

    User create(String invitationToken, User user);

    User createAdministrator();

    void patch(User user);

    /**
     * Update password without matching old and new passwords.
     * Basically it's used when user has "iam:users:update" permission.
     */
    void forceUpdatePassword(Integer id, String newPassword);

    void updatePassword(Integer id, String oldPassword, String newPassword);

    void updatePassword(String resetToken, String newPassword);

    void updateLastLogin(Integer id, Instant lastLogin);

    void addUserToGroup(Integer userId, Integer groupId);

    void removeUserFromGroup(Integer userId, Integer groupId);

    void sendResetPasswordEmail(String email);

    void deleteById(Integer id);

}

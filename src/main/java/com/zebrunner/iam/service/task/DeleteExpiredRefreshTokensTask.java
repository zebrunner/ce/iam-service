package com.zebrunner.iam.service.task;

import com.zebrunner.iam.service.RefreshTokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeleteExpiredRefreshTokensTask {

    private final RefreshTokenService refreshTokenService;

    @Scheduled(cron = "${service.task.cron.tokens-cleanup}", zone = "UTC")
    public void cleanupTokens() {
        try {
            log.info("Expired tokens cleanup task has been started.");
            Instant expirationDate = Instant.now();
            refreshTokenService.deleteExpired(expirationDate);
            log.info("Expired tokens cleanup task has been successfully completed.");
        } catch (Exception e) {
            log.error("Could not complete expired tokens cleanup task.", e);
        }
    }

}

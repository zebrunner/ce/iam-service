package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.token.AuthenticationData;

public interface AuthenticationService {

    AuthenticationData authenticate(String username, String password, String userAgentHeader, String ipAddress);

    AuthenticationData refresh(String token, String userAgentHeader, String ipAddress);

}

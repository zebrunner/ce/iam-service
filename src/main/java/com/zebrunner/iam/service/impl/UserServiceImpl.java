package com.zebrunner.iam.service.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Invitation;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserSource;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.UserSearchCriteria;
import com.zebrunner.iam.messaging.sender.EmailSender;
import com.zebrunner.iam.messaging.sender.UserDeletedSender;
import com.zebrunner.iam.messaging.sender.UserSavedSender;
import com.zebrunner.iam.persistance.UserRepository;
import com.zebrunner.iam.service.ApiTokenService;
import com.zebrunner.iam.service.GroupService;
import com.zebrunner.iam.service.InvitationService;
import com.zebrunner.iam.service.PreferenceService;
import com.zebrunner.iam.service.RefreshTokenService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.config.AdministratorProperties;
import com.zebrunner.iam.service.config.TokenProperties;
import com.zebrunner.iam.service.exception.BusinessConstraintError;
import com.zebrunner.iam.service.exception.InvocationLimitExceededError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final static String ANONYMOUS_USERNAME = "anonymous";
    private final static String ADMIN_USERNAME = "admin";

    private final EmailSender emailSender;
    private final GroupService groupService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenProperties tokenProperties;
    private final UserSavedSender userSavedSender;
    private final UserDeletedSender userDeletedSender;
    private final InvitationService invitationService;
    private final PreferenceService preferenceService;
    private final RefreshTokenService refreshTokenService;
    private final ApiTokenService apiTokenService;
    private final AdministratorProperties administratorProperties;

    @Override
    @Transactional(readOnly = true)
    public SearchResult<User> search(UserSearchCriteria searchCriteria) {
        return userRepository.findBySearchCriteria(searchCriteria);
    }

    @Override
    public Optional<User> getById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> getWithPermissionsById(Integer id) {
        return userRepository.findWithPermissionsById(id);
    }

    @Override
    public Optional<User> getWithPermissionsByUsername(String username) {
        return userRepository.findWithPermissionsByUsername(username);
    }

    @Override
    public Optional<User> getByUsernameOrEmail(String usernameOrEmail) {
        return userRepository.findByUsernameOrEmail(usernameOrEmail);
    }

    @Override
    public Optional<User> getByResetToken(String resetToken) {
        return userRepository.findByResetToken(resetToken);
    }

    @Override
    public List<User> getByIds(List<Integer> ids) {
        return userRepository.findAllByIdIn(ids);
    }

    @Override
    @Transactional
    public User create(User user) {
        if (userRepository.existsByUsernameOrEmail(user.getUsername(), user.getEmail())) {
            throw BusinessConstraintError.USERNAME_OR_EMAIL_ALREADY_EXISTS.withoutArgs();
        }

        user.setId(null);
        user.setOnboardingCompleted(false);
        user.setStatus(UserStatus.ACTIVE);
        user.setSource(UserSource.INTERNAL);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (user.getGroups().isEmpty() && user.getPermissions().isEmpty()) {
            groupService.getDefault()
                        .map(Set::of)
                        .ifPresent(user::setGroups);
        }

        user = userRepository.save(user);
        preferenceService.resetToDefault(user.getId());

        userSavedSender.sendNotification(user);

        return user;
    }

    @Override
    @Transactional
    public User create(String invitationToken, User user) {
        String username = user.getUsername();
        if (userRepository.existsByUsername(username)) {
            throw BusinessConstraintError.USERNAME_ALREADY_EXISTS.withArgs(username);
        }

        Invitation invitation = invitationService.getByToken(invitationToken)
                                                 .orElseThrow(ResourceNotFoundError.INVITATION_NOT_FOUND_BY_TOKEN::withoutArgs);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Integer groupId = invitation.getGroupId();
        Group group = groupService.getById(groupId)
                                  .orElseThrow(() -> ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(groupId));

        user.setGroups(Set.of(group));
        user.setStatus(UserStatus.ACTIVE);
        user.setSource(invitation.getSource());
        user.setOnboardingCompleted(false);

        invitationService.deleteById(invitation.getId());
        user = userRepository.save(user);
        preferenceService.resetToDefault(user.getId());

        userSavedSender.sendNotification(user);

        return user;
    }

    @Override
    @Transactional
    public User createAdministrator() {
        return userRepository.findByUsernameOrEmail(administratorProperties.getUsername())
                             .orElseGet(this::createNewAdministrator);
    }

    private User createNewAdministrator() {
        String groupName = administratorProperties.getGroup();
        Group group = groupService.getByName(groupName)
                                  .orElseThrow(() -> ResourceNotFoundError.GROUP_NOT_FOUND_BY_NAME.withArgs(groupName));

        User user = new User();
        user.setUsername(administratorProperties.getUsername());
        user.setPassword(passwordEncoder.encode(administratorProperties.getPassword()));
        user.setGroups(Set.of(group));
        user.setStatus(UserStatus.ACTIVE);
        user.setSource(UserSource.INTERNAL);
        user.setOnboardingCompleted(true);

        user = userRepository.save(user);
        preferenceService.resetToDefault(user.getId());

        userSavedSender.sendNotification(user);

        return user;
    }

    @Override
    @Transactional
    public void patch(User userPatch) {
        Integer userId = userPatch.getId();
        User user = userRepository.findById(userId)
                                  .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));

        if (userPatch.getEmail() != null) {
            if (userRepository.existsByEmailAndIdNot(userPatch.getEmail(), userPatch.getId())) {
                throw BusinessConstraintError.USER_EMAIL_ALREADY_EXISTS.withoutArgs();
            }
            user.setEmail(userPatch.getEmail());
        }

        setIfNotNull(userPatch.getStatus(), user::setStatus);
        setIfNotNull(userPatch.getFirstName(), user::setFirstName);
        setIfNotNull(userPatch.getLastName(), user::setLastName);
        setIfNotNull(userPatch.getPhotoUrl(), user::setPhotoUrl);
        setIfNotNull(userPatch.getOnboardingCompleted(), user::setOnboardingCompleted);

        if (user.getStatus() == UserStatus.INACTIVE) {
            refreshTokenService.revokeAll(user.getId().longValue());
            apiTokenService.revokeAll(user.getId().longValue());
        }
        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    private <T> void setIfNotNull(T value, Consumer<T> setter) {
        if (value != null) {
            setter.accept(value);
        }
    }

    @Override
    @Transactional
    public void forceUpdatePassword(Integer id, String newPassword) {
        User user = userRepository.findById(id)
                                  .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(id));

        user.setPassword(passwordEncoder.encode(newPassword));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    @Transactional
    public void updatePassword(Integer id, String oldPassword, String newPassword) {
        User user = userRepository.findById(id)
                                  .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(id));

        if (oldPassword == null || !passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw BusinessConstraintError.WRONG_OLD_PASSWORD.withoutArgs();
        }

        user.setPassword(passwordEncoder.encode(newPassword));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    @Transactional
    public void updatePassword(String resetToken, String newPassword) {
        User user = userRepository.findByResetToken(resetToken)
                                  .orElseThrow(ResourceNotFoundError.USER_NOT_FOUND_BY_RESET_TOKEN::withoutArgs);
        user.setResetToken(null);
        user.setPassword(passwordEncoder.encode(newPassword));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    public void updateLastLogin(Integer id, Instant lastLogin) {
        userRepository.updateLastLogin(id, lastLogin);
    }

    @Override
    public void addUserToGroup(Integer userId, Integer groupId) {
        User user = userRepository.findWithPermissionsById(userId)
                                  .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        if (hasGroup(user, groupId)) {
            throw BusinessConstraintError.USER_HAS_ADDED_TO_GROUP.withoutArgs();
        }

        Group group = groupService.getById(groupId)
                                  .orElseThrow(() -> ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(groupId));
        user.getGroups().add(group);

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    @Override
    public void removeUserFromGroup(Integer userId, Integer groupId) {
        User user = userRepository.findWithPermissionsById(userId)
                                  .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        if (!hasGroup(user, groupId)) {
            throw BusinessConstraintError.USER_HAS_NOT_ADDED_TO_GROUP.withArgs(groupId);
        }

        user.getGroups().removeIf(group -> group.getId().equals(groupId));

        user = userRepository.save(user);
        userSavedSender.sendNotification(user);
    }

    private boolean hasGroup(User user, Integer groupId) {
        return user.getGroups()
                   .stream()
                   .anyMatch(group -> group.getId().equals(groupId));
    }

    @Override
    @Transactional
    public void sendResetPasswordEmail(String email) {
        User user = userRepository.findByEmail(email)
                                  .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_EMAIL.withArgs(email));

        if (UserSource.INTERNAL == user.getSource()) {
            if (isNotValidResetToken(user.getResetTokenValidFrom())) {
                throw InvocationLimitExceededError.OFTEN_PASSWORD_RESETS.withoutArgs();
            }

            String resetToken = user.getResetToken() != null
                    ? user.getResetToken()
                    : RandomStringUtils.randomAlphabetic(tokenProperties.getPasswordReset().getChars());
            user.setResetToken(resetToken);
            user.setResetTokenValidFrom(Instant.now());

            user = userRepository.save(user);
            userSavedSender.sendNotification(user);

            emailSender.sendForgotPasswordEmail(resetToken, email);
        }
    }

    private boolean isNotValidResetToken(Instant validFrom) {
        int retryDelay = tokenProperties.getPasswordReset().getRetryInSecs();
        return validFrom != null && !Instant.now().minusSeconds(retryDelay).isAfter(validFrom);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        User user = userRepository.findById(id)
                                  .filter(existingUser -> !Objects.equals(existingUser.getUsername(), ANONYMOUS_USERNAME))
                                  .filter(existingUser -> !Objects.equals(existingUser.getUsername(), ADMIN_USERNAME))
                                  .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.USER_NOT_FOUND_BY_ID, id));

        userRepository.deleteById(id);
        userDeletedSender.sendNotification(user);
    }
}

package com.zebrunner.iam.service.impl;

import com.zebrunner.common.auth.token.TokenPayload;
import com.zebrunner.common.auth.user.AuthenticatedUser;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.iam.domain.entity.ApiToken;
import com.zebrunner.iam.domain.entity.RefreshToken;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.token.AuthenticationData;
import com.zebrunner.iam.domain.token.RefreshTokenContent;
import com.zebrunner.iam.service.ApiTokenService;
import com.zebrunner.iam.service.AuthenticationService;
import com.zebrunner.iam.service.JwtService;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.RefreshTokenService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.config.TokenProperties;
import com.zebrunner.iam.service.exception.AuthenticationError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import com.zebrunner.iam.service.exception.UnauthorizedAccessError;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final JwtService jwtService;
    private final UserService userService;
    private final TokenProperties tokenProperties;
    private final PermissionService permissionService;
    private final AuthenticationManager dbAuthenticationManager;
    private final RefreshTokenService refreshTokenService;
    private final ApiTokenService apiTokenService;

    @Override
    @Transactional
    public AuthenticationData authenticate(String username, String password, String userAgentHeader, String ipAddress) {
        User user = userService.getByUsernameOrEmail(username)
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_USERNAME.withArgs(username));

        Set<String> permissions = Set.of();
        try {
            Authentication authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), password);

            Authentication authentication = dbAuthenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            if (authentication.getPrincipal() instanceof AuthenticatedUser) {
                AuthenticatedUser authenticatedUser = (AuthenticatedUser) authentication.getPrincipal();
                permissions = authenticatedUser.getPermissions();
            }
        } catch (AuthenticationException e) {
            throw AuthenticationError.INVALID_USER_CREDENTIALS.withoutArgs();
        }

        userService.updateLastLogin(user.getId(), Instant.now());
        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        return generateAuthToken(user, permissions, expirationInSecs, userAgentHeader, ipAddress);
    }

    @Override
    @Transactional
    public AuthenticationData refresh(String token, String userAgentHeader, String ipAddress) {
        TokenContent tokenContent;
        if (token.contains(".")) {
            tokenContent = getJwtTokenContent(token);
        } else {
            tokenContent = refreshTokenService.retrieveOptionalByValue(token)
                                              .map(this::getRefreshTokenContent)
                                              .or(() -> apiTokenService.retrieveOptionalByValue(token)
                                                                       .map(this::getApiTokenContent))
                                              .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TOKEN_NOT_FOUND_BY_VALUE));
        }
        User user = tokenContent.getUser();
        Integer expirationInSecs = tokenContent.getExpirationInSecs();

        Set<String> permissionsSuperset = permissionService.getSuperset(user.getGroups(), user.getPermissions());

        return generateAuthToken(user, permissionsSuperset, expirationInSecs, userAgentHeader, ipAddress);
    }

    private TokenContent getApiTokenContent(ApiToken apiToken) {
        Long userId = apiToken.getUserId();
        Instant expiresAt = apiToken.getExpiresAt();
        if (expiresAt != null && expiresAt.isBefore(Instant.now())) {
            throw AuthenticationError.TOKEN_IS_EXPIRED.withoutArgs();
        }
        User user = userService.getWithPermissionsById(userId.intValue())
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        if (UserStatus.INACTIVE.equals(user.getStatus())) {
            throw UnauthorizedAccessError.USER_NOT_ACTIVE.withArgs(user);
        }

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getReportingClient();
        apiTokenService.updateLastUsed(user.getId().longValue(), Instant.now());

        return TokenContent.builder()
                           .expirationInSecs(expirationInSecs)
                           .user(user)
                           .build();
    }

    private TokenContent getRefreshTokenContent(RefreshToken refreshToken) {
        Long userId = refreshToken.getUserId();
        Instant expiresAt = refreshToken.getExpiresAt();
        if (expiresAt != null && expiresAt.isBefore(Instant.now())) {
            throw AuthenticationError.TOKEN_IS_EXPIRED.withoutArgs();
        }
        User user = userService.getWithPermissionsById(userId.intValue())
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        if (UserStatus.INACTIVE.equals(user.getStatus())) {
            throw UnauthorizedAccessError.USER_NOT_ACTIVE.withArgs(user);
        }

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        userService.updateLastLogin(user.getId(), Instant.now());

        return TokenContent.builder()
                           .expirationInSecs(expirationInSecs)
                           .user(user)
                           .build();
    }

    private TokenContent getJwtTokenContent(String token) {
        RefreshTokenContent refreshTokenContent = jwtService.parseRefreshToken(token);
        if (refreshTokenContent.getExpiration().isBefore(Instant.now())) {
            throw AuthenticationError.TOKEN_IS_EXPIRED.withoutArgs();
        }
        Integer userId = refreshTokenContent.getUserId();
        User user = userService.getWithPermissionsById(userId)
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        if (UserStatus.INACTIVE.equals(user.getStatus())) {
            throw UnauthorizedAccessError.USER_NOT_ACTIVE.withArgs(userId);
        }
        if (!Objects.equals(user.getPassword(), refreshTokenContent.getPassword())) {
            throw AuthenticationError.USER_PASSWORD_HAS_CHANGED.withoutArgs();
        }
        // check if the provided token is the access token (access token has greater expiration period then auth one)
        int refreshTokenExpiration = tokenProperties.getAuth().getExpirationInSecs().getRefresh();
        Instant authTokenMaxExpiration = Instant.now().plusSeconds(refreshTokenExpiration);
        boolean isAccessTokenRefresh = authTokenMaxExpiration.isBefore(refreshTokenContent.getExpiration());

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        if (isAccessTokenRefresh) {
            userService.updateLastLogin(user.getId(), Instant.now());
            expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getRefresh();
        }

        return TokenContent.builder()
                           .expirationInSecs(expirationInSecs)
                           .user(user)
                           .build();
    }

    private AuthenticationData generateAuthToken(User user,
                                                 Set<String> permissionsSuperset,
                                                 Integer expirationInSecs,
                                                 String userAgentHeader,
                                                 String ipAddress) {
        Integer userId = user.getId();
        String username = user.getUsername();

        String authToken = jwtService.generateAuthToken(userId, username, permissionsSuperset, expirationInSecs);
        RefreshToken refreshToken = refreshTokenService.generate(userId.longValue(), userAgentHeader, ipAddress);
        return AuthenticationData.builder()
                                 .userId(userId)
                                 .userSource(user.getSource())
                                 .permissionsSuperset(permissionsSuperset)
                                 .previousLogin(user.getLastLogin())
                                 .authTokenType("Bearer")
                                 .authToken(authToken)
                                 .authTokenExpirationInSecs(expirationInSecs)
                                 .refreshToken(refreshToken.getValue())
                                 .build();
    }

    @Data
    @Builder
    static class TokenContent {

        User user;
        Integer expirationInSecs;

    }

}

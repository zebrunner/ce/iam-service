package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.persistance.GroupRepository;
import com.zebrunner.iam.service.GroupService;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.exception.BusinessConstraintError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import com.zebrunner.iam.service.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final PermissionService permissionService;

    @Override
    public SearchResult<Group> search(SearchCriteria searchCriteria) {
        return groupRepository.findBySearchCriteria(searchCriteria);
    }

    @Override
    public Optional<Group> getDefault() {
        return groupRepository.findByIsDefaultTrue();
    }

    @Override
    public Optional<Group> getById(Integer id) {
        return groupRepository.findWithPermissionsById(id);
    }

    @Override
    public Optional<Group> getByName(String name) {
        return groupRepository.findByName(name);
    }

    @Override
    public Optional<Group> findInvitableById(Integer id) {
        return groupRepository.findByInvitableTrueAndId(id);
    }

    @Override
    @Transactional
    public Group save(Group group) {
        checkGroupId(group.getId());
        checkGroupName(group);

        Set<String> permissionNames = StreamUtils.mapToSet(group.getPermissions(), Permission::getName);
        Set<Permission> foundPermissions = permissionService.getConciseSetByNames(permissionNames);
        group.setPermissions(foundPermissions);

        if (group.getIsDefault()) {
            groupRepository.setAllNotDefault();
            groupRepository.refresh(group);
        }
        group = groupRepository.save(group);
        return group;
    }

    private void checkGroupId(Integer id) {
        if (id != null && !groupRepository.existsById(id)) {
            throw ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(id);
        }
    }

    private void checkGroupName(Group group) {
        String name = group.getName();
        groupRepository.findByName(name)
                       .map(Group::getId)
                       .filter(Predicate.not(id -> id.equals(group.getId())))
                       .ifPresent(id -> {
                           throw ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(id);
                       });
    }

    @Override
    @Transactional
    public void setDefault(Integer id, boolean isDefault) {
        Group group = groupRepository.findById(id)
                                     .orElseThrow(() -> ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(id));
        if (group.getIsDefault() != isDefault) {
            groupRepository.setAllNotDefault();
            group.setIsDefault(isDefault);

            groupRepository.save(group);
        }
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        Group group = groupRepository.findById(id)
                                     .orElseThrow(() -> ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(id));

        if (groupRepository.countUsersById(id) != 0) {
            throw BusinessConstraintError.GROUP_HAS_ASSIGNED_USERS.withoutArgs();
        }

        groupRepository.delete(group);
    }

}

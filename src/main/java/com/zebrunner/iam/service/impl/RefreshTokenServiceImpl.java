package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.RefreshToken;
import com.zebrunner.iam.persistance.RefreshTokenRepository;
import com.zebrunner.iam.service.RefreshTokenService;
import com.zebrunner.iam.service.config.TokenProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final TokenProperties tokenProperties;
    private final RefreshTokenRepository refreshTokenRepository;

    @Override
    @Transactional(readOnly = true)
    public Optional<RefreshToken> retrieveOptionalByValue(String value) {
        return refreshTokenRepository.findByValue(value);
    }

    @Override
    @Transactional
    public RefreshToken generate(Long userId, String userAgentHeader, String ipAddress) {
        String value = RandomStringUtils.randomAlphanumeric(50);
        int refreshTokenExpiration = tokenProperties.getAuth().getExpirationInSecs().getRefresh();
        Instant expiresAt = Instant.now().plusSeconds(refreshTokenExpiration);
        RefreshToken refreshToken = RefreshToken.builder()
                                                .value(value)
                                                .expiresAt(expiresAt)
                                                .userAgentHeader(userAgentHeader)
                                                .ipAddress(ipAddress)
                                                .userId(userId)
                                                .build();
        RefreshToken savedRefreshToken = refreshTokenRepository.save(refreshToken);
        entityManager.detach(savedRefreshToken);
        savedRefreshToken.setValue(value);
        return savedRefreshToken;
    }

    @Override
    @Transactional
    public void revokeAll(Long userId) {
        refreshTokenRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteExpired(Instant expirationDate) {
        refreshTokenRepository.deleteByExpiresAtBefore(expirationDate);
    }

}

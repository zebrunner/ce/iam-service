package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Preference;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.persistance.PreferenceRepository;
import com.zebrunner.iam.persistance.UserRepository;
import com.zebrunner.iam.service.PreferenceService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.exception.BusinessConstraintError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PreferenceServiceImpl implements PreferenceService {

    private final UserRepository userRepository;
    private final PreferenceRepository preferenceRepository;

    private static final Set<String> ALLOWED_PREFERENCES = Set.of(
            "REFRESH_INTERVAL", "DEFAULT_TEST_VIEW", "THEME"
    );

    @Override
    public List<Preference> getByUserId(Integer userId) {
        if (!userRepository.existsById(userId)) {
            throw ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId);
        }
        return preferenceRepository.findByUserId(userId);
    }

    @Override
    @Transactional
    public List<Preference> saveAll(Integer userId, List<Preference> preferences) {
        List<Preference> existingPreferences = getByUserId(userId);
        Map<String, Preference> nameToExistingPreference = mapNameToIdentity(existingPreferences);

        User user = User.builder().id(userId).build();
        preferences = preferences.stream()
                                 .peek(preference -> preference.setId(null))
                                 .map(preference -> getValid(preference, nameToExistingPreference))
                                 .peek(preference -> preference.setUser(user))
                                 .collect(Collectors.toList());
        return preferenceRepository.saveAll(preferences);
    }

    private Map<String, Preference> mapNameToIdentity(Collection<Preference> preferences) {
        return preferences.stream()
                          .collect(Collectors.toMap(Preference::getName, Function.identity()));
    }

    private Preference getValid(Preference preference, Map<String, Preference> nameToExistingPreference) {
        String preferenceName = preference.getName();
        if (!ALLOWED_PREFERENCES.contains(preferenceName)) {
            throw BusinessConstraintError.INCORRECT_PREFERENCE_NAME.withArgs(preferenceName);
        }

        Preference existingPreference = nameToExistingPreference.get(preferenceName);
        if (existingPreference != null) {
            existingPreference.setValue(preference.getValue());
            return existingPreference;
        } else {
            return preference;
        }
    }

    @Override
    @Transactional
    public List<Preference> resetToDefault(Integer userId) {
        List<Preference> preferences = preferenceRepository.findByUserUsername(UserService.ANONYMOUS);
        preferences = preferences.stream()
                                 .map(preference -> new Preference(preference.getName(), preference.getValue()))
                                 .collect(Collectors.toList());
        return saveAll(userId, preferences);
    }

}

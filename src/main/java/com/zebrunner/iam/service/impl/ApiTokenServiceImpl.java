package com.zebrunner.iam.service.impl;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.iam.domain.entity.ApiToken;
import com.zebrunner.iam.persistance.ApiTokenRepository;
import com.zebrunner.iam.service.ApiTokenService;
import com.zebrunner.iam.service.exception.BusinessConstraintError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApiTokenServiceImpl implements ApiTokenService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final ApiTokenRepository apiTokenRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ApiToken> retrieveByUserId(Long userId) {
        return apiTokenRepository.findAllByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public ApiToken retrieveById(Long id) {
        return apiTokenRepository.findById(id)
                                 .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.API_TOKEN_NOT_FOUND_BY_ID, id));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ApiToken> retrieveOptionalByValue(String value) {
        return apiTokenRepository.findByValue(value);
    }

    @Override
    @Transactional
    public ApiToken generate(String name, Instant expiresAt, Long userId) {
        String value = RandomStringUtils.randomAlphanumeric(50);
        boolean duplicateName = apiTokenRepository.existsByUserIdAndName(userId, name);
        if (duplicateName) {
            throw new BusinessConstraintException(BusinessConstraintError.API_TOKEN_NAME_ALREADY_EXISTS_FOR_USER, name, userId);
        }
        ApiToken apiToken = ApiToken.builder()
                                    .name(name)
                                    .value(value)
                                    .expiresAt(expiresAt)
                                    .userId(userId)
                                    .build();
        ApiToken savedApiToken = apiTokenRepository.save(apiToken);
        // For the first time we should return decrypted value, though not modifying db entity
        entityManager.detach(savedApiToken);
        savedApiToken.setValue(value);
        return savedApiToken;
    }

    @Override
    @Transactional
    public void updateLastUsed(Long id, Instant lastLogin) {
        apiTokenRepository.updateLastUsed(id, lastLogin);
    }

    @Override
    @Transactional
    public void revoke(Long id) {
        boolean exists = apiTokenRepository.existsById(id);
        if (!exists) {
            throw new ResourceNotFoundException(ResourceNotFoundError.API_TOKEN_NOT_FOUND_BY_ID, id);
        }
        apiTokenRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void revokeAll(Long userId) {
        apiTokenRepository.deleteAllByUserId(userId);
    }

}

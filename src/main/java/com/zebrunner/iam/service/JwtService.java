package com.zebrunner.iam.service;

import com.zebrunner.common.auth.token.TokenPayload;
import com.zebrunner.iam.domain.token.RefreshTokenContent;

import java.util.Set;

public interface JwtService {

    String generateAuthToken(Integer userId, String username, Set<String> permissions, int expirationInSecs);

    TokenPayload parseAuthToken(String token);

    RefreshTokenContent parseRefreshToken(String token);

}

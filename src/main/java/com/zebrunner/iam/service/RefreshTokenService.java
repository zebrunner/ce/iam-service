package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.RefreshToken;

import java.time.Instant;
import java.util.Optional;

public interface RefreshTokenService {

    Optional<RefreshToken> retrieveOptionalByValue(String value);

    RefreshToken generate(Long userId, String userAgentHeader, String ipAddress);

    void revokeAll(Long userId);

    void deleteExpired(Instant expirationDate);

}

package com.zebrunner.iam.service.config;

import com.zebrunner.iam.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

// TODO: 3/18/21 dkazak Remove this class next Release

@Slf4j
@Component
@RequiredArgsConstructor
public class DeleteLdapUsersTask implements CommandLineRunner {

    private final JdbcTemplate jdbcTemplate;

    private final UserService userService;

    @Override
    @Transactional
    public void run(String... args) {
        log.info("Start running delete Ldap users task");
        String selectLdapUserIdsQuery = "SELECT id FROM users WHERE source = 'LDAP'";
        List<Integer> ldapUserIds = jdbcTemplate.queryForList(selectLdapUserIdsQuery, Integer.class);
        log.info("Count ldap users to delete: {}", ldapUserIds.size());
        ldapUserIds.forEach(this::deleteLdapUserById);
        log.info("End running delete Ldap users task");
    }

    private void deleteLdapUserById(Integer id) {
        log.info("Start deleting ldap user with id: {}", id);
        try {
            userService.deleteById(id);
            log.info("End deleting ldap user with id: {}", id);
        } catch (Exception e) {
            log.error("Could not delete ldap user by id: {}", id, e);
        }
    }

}

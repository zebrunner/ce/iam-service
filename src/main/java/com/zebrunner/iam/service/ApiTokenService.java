package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.ApiToken;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface ApiTokenService {

    List<ApiToken> retrieveByUserId(Long userId);

    Optional<ApiToken> retrieveOptionalByValue(String value);

    ApiToken retrieveById(Long id);

    ApiToken generate(String name, Instant expiresAt, Long userId);

    void updateLastUsed(Long id, Instant lastLogin);

    void revoke(Long id);

    void revokeAll(Long userId);

}

package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.Preference;

import java.util.List;

public interface PreferenceService {

    List<Preference> getByUserId(Integer userId);

    List<Preference> saveAll(Integer userId, List<Preference> preferences);

    List<Preference> resetToDefault(Integer userId);

}

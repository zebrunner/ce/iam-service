package com.zebrunner.iam.service.exception;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BusinessConstraintError implements com.zebrunner.common.eh.error.BusinessConstraintError {

    GROUP_NAME_ALREADY_EXISTS(3200),
    GROUP_HAS_ASSIGNED_USERS(3201),
    USERNAME_ALREADY_EXISTS(3202),
    USER_EMAIL_ALREADY_EXISTS(3203),
    USERNAME_OR_EMAIL_ALREADY_EXISTS(3204),
    USER_NOT_FOUND_IN_LDAP(3205),
    WRONG_OLD_PASSWORD(3206),
    USER_HAS_ADDED_TO_GROUP(3207),
    USER_HAS_NOT_ADDED_TO_GROUP(3208),
    INCORRECT_PREFERENCE_NAME(3209),
    API_TOKEN_NAME_ALREADY_EXISTS_FOR_USER(3220),
    USER_API_TOKEN_REVOCATION_IS_NOT_PERMITTED(3221),
    ACCESS_TO_USER_API_TOKENS_IS_NOT_PERMITTED(3222);

    private final int code;

    public BusinessConstraintException withoutArgs() {
        return new BusinessConstraintException(this);
    }

    public BusinessConstraintException withArgs(Object... messageArgs) {
        return new BusinessConstraintException(this, messageArgs);
    }

}

package com.zebrunner.iam.service.exception;

import com.zebrunner.common.eh.exception.UnauthorizedAccessException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UnauthorizedAccessError implements com.zebrunner.common.eh.error.UnauthorizedAccessError {

    USER_NOT_ACTIVE(3600);

    private final int code;

    public UnauthorizedAccessException withArgs(Object... messageArgs) {
        return new UnauthorizedAccessException(this, messageArgs);
    }

}

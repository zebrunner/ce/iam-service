package com.zebrunner.iam.service.exception;

import com.zebrunner.common.eh.exception.MalformedInputException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MalformedInputError implements com.zebrunner.common.eh.error.MalformedInputError {

    REQUIRED_VALUE_NOT_PROVIDED(3400),
    PATCH_REQUEST_OPERATION_NOT_SUPPORTED_FOR_PATH(3401),
    PATCH_REQUEST_VALUE_HAS_WRONG_TYPE(3402);

    private final int code;

    public MalformedInputException withoutArgs() {
        return new MalformedInputException(this);
    }

    public MalformedInputException withArgs(Object... messageArgs) {
        return new MalformedInputException(this, messageArgs);
    }

}

package com.zebrunner.iam.service.exception;

import com.zebrunner.common.eh.exception.AuthenticationException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum AuthenticationError implements com.zebrunner.common.eh.error.AuthenticationError {

    INVALID_USER_CREDENTIALS(3100),
    USER_PASSWORD_HAS_CHANGED(3102),
    INVALID_AUTHENTICATION(3103),
    TOKEN_IS_EXPIRED(3105);

    private final int code;

    public AuthenticationException withoutArgs() {
        return new AuthenticationException(this);
    }

}

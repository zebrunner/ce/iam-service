package com.zebrunner.iam.messaging.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Exchange {

    public static final String USER_SAVED = "user.saved";
    public static final String USER_DELETED = "user.deleted";
    public static final String EMAIL = "email";

}

package com.zebrunner.iam.messaging.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoutingKey {

    public static final String SEND_EMAIL = "send";

}

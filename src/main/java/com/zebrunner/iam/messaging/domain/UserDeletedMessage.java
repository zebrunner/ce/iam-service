package com.zebrunner.iam.messaging.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserDeletedMessage {

    Integer id;
    String name;

}

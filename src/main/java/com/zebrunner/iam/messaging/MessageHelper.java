package com.zebrunner.iam.messaging;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class MessageHelper {

    private final RabbitTemplate rabbitTemplate;

    public void send(String routingKey, Object message, Map<String, ?> metadata) {
        rabbitTemplate.convertAndSend(routingKey, message, rabbitmqMessage -> {
            metadata.forEach((key, value) -> rabbitmqMessage.getMessageProperties().setHeader(key, value));
            return rabbitmqMessage;
        });
    }

    public void send(String exchange, String routingKey, Object message) {
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

    public void send(String exchange, Object message) {
        send(exchange, "", message);
    }

}

package com.zebrunner.iam.messaging.sender;

import com.zebrunner.iam.messaging.MessageHelper;
import com.zebrunner.iam.messaging.config.Exchange;
import com.zebrunner.iam.messaging.config.RoutingKey;
import com.zebrunner.iam.messaging.domain.MailDataMessage;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Slf4j
@Component
@RequiredArgsConstructor
public class EmailSender {

    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    private static final String USER_INVITATION_TEMPLATE_NAME = "invitation.ftl";
    private static final String USER_INVITATION_MAIL_SUBJECT = "Join the workspace";

    private static final String FORGOT_PASSWORD_TEMPLATE_NAME = "forgot_password.ftl";
    private static final String FORGOT_PASSWORD_MAIL_SUBJECT = "Password reset";

    @Setter(onMethod = @__(@Value("${zebrunner.host}")))
    private String zebrunnerHost;

    private final MessageHelper messageHelper;

    public void sendUserInvitationEmail(String token, String... emails) {
        Map<String, Object> templateModel = Map.of("token", token, "zebrunnerHost", zebrunnerHost);
        MailDataMessage mailDataMessage = MailDataMessage.builder()
                                                         .templateName(USER_INVITATION_TEMPLATE_NAME)
                                                         .subject(USER_INVITATION_MAIL_SUBJECT)
                                                         .toEmails(Set.of(emails))
                                                         .templateModel(templateModel)
                                                         .build();
        sendEmail(mailDataMessage);
    }

    public void sendForgotPasswordEmail(String resetToken, String... emails) {
        Map<String, Object> templateModel = Map.of("token", resetToken, "zebrunnerHost", zebrunnerHost);
        MailDataMessage mailDataMessage = MailDataMessage.builder()
                                                         .templateName(FORGOT_PASSWORD_TEMPLATE_NAME)
                                                         .subject(FORGOT_PASSWORD_MAIL_SUBJECT)
                                                         .toEmails(Set.of(emails))
                                                         .templateModel(templateModel)
                                                         .build();
        sendEmail(mailDataMessage);
    }

    private void sendEmail(MailDataMessage mailDataMessage) {
        Set<String> validRecipients = processRecipients(mailDataMessage.getToEmails());
        if (!validRecipients.isEmpty()) {
            mailDataMessage.toBuilder().toEmails(validRecipients);
            messageHelper.send(Exchange.EMAIL, RoutingKey.SEND_EMAIL, mailDataMessage);
        }
    }

    private Set<String> processRecipients(Set<String> emails) {
        return emails.stream().filter(email -> {
            boolean isValid = isValid(email);
            if (!isValid) {
                log.info("Not valid recipient specified: " + email);
            }
            return isValid;
        }).collect(Collectors.toSet());
    }

    private boolean isValid(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

}

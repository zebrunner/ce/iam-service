package com.zebrunner.iam.messaging.sender;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.messaging.MessageHelper;
import com.zebrunner.iam.messaging.config.Exchange;
import com.zebrunner.iam.messaging.domain.UserDeletedMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserDeletedSender {

    private final MessageHelper messageHelper;

    public void sendNotification(User user) {
        UserDeletedMessage message = UserDeletedMessage.builder()
                                                       .id(user.getId())
                                                       .name(user.getUsername())
                                                       .build();

        messageHelper.send(Exchange.USER_DELETED, message);
    }

}

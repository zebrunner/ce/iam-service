package com.zebrunner.iam.web.patch;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PatchEmailValidator implements ConstraintValidator<ValidEmailItem, PatchRequest> {

    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    @Override
    public boolean isValid(PatchRequest request, ConstraintValidatorContext context) {
        boolean isValid = true;
        for (int i = 0, requestSize = request.size(); i < requestSize; i++) {
            PatchRequest.Item item = request.get(i);
            if (!isValidItem(item)) {
                context.disableDefaultConstraintViolation();

                String message = context.getDefaultConstraintMessageTemplate();
                context.buildConstraintViolationWithTemplate(message)
                       .addPropertyNode(String.format("[%d].value", i))
                       .addConstraintViolation();
                isValid = false;
            }
        }
        return isValid;
    }

    private boolean isValidItem(PatchRequest.Item item) {
        boolean valueIsNull = item.getValue() == null;
        boolean notEmail = item.getPath().isBlank() || !item.getPath().equals("/email");

        String value = item.getValue().toString();
        return valueIsNull || notEmail || EMAIL_PATTERN.matcher(value).matches();
    }
}

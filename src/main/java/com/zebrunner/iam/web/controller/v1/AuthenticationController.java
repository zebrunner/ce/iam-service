package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.iam.domain.entity.ApiToken;
import com.zebrunner.iam.domain.token.AuthenticationData;
import com.zebrunner.iam.service.ApiTokenService;
import com.zebrunner.iam.service.AuthenticationService;
import com.zebrunner.iam.service.JwtService;
import com.zebrunner.iam.web.annotation.UserIpAddress;
import com.zebrunner.iam.web.documentation.v1.AuthenticationDocumentedController;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.request.v1.LoginRequest;
import com.zebrunner.iam.web.request.v1.RefreshAuthTokenRequest;
import com.zebrunner.iam.web.response.v1.AuthenticationDataResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Instant;
import java.util.Map;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController implements AuthenticationDocumentedController {

    private static final String FIRST_LOGIN_HEADER = "x-zbr-first-login";

    private final Mapper mapper;
    private final JwtService jwtService;
    private final ApiTokenService apiTokenService;
    private final AuthenticationService authenticationService;

    @Override
    @PostMapping("/login")
    public ResponseEntity<AuthenticationDataResponse> login(@RequestBody @Valid LoginRequest loginRequest,
                                                            @RequestHeader(value = "User-Agent", required = false) String userAgentHeader,
                                                            @UserIpAddress String ipAddress) {
        String username = loginRequest.getUsername();
        String password = loginRequest.getPassword();
        AuthenticationData authenticationData = authenticationService.authenticate(username, password, userAgentHeader, ipAddress);

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        builder.header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, FIRST_LOGIN_HEADER)
               .header(FIRST_LOGIN_HEADER, Boolean.toString(authenticationData.getPreviousLogin() == null));

        return builder.body(mapper.map(authenticationData, AuthenticationDataResponse.class));
    }

    @Override
    @PostMapping("/refresh")
    public AuthenticationDataResponse refresh(@RequestBody RefreshAuthTokenRequest refreshAuthTokenRequest,
                                              @RequestHeader(value = "User-Agent", required = false) String userAgentHeader,
                                              @UserIpAddress String ipAddress) {
        String refreshToken = refreshAuthTokenRequest.getRefreshToken();
        AuthenticationData authenticationData = authenticationService.refresh(refreshToken, userAgentHeader, ipAddress);
        return mapper.map(authenticationData, AuthenticationDataResponse.class);
    }

    @Override
    @GetMapping("/access")
    public Map<String, String> getApiToken(@PrincipalId Integer userId) {
        String tokenName = "Token-" + Instant.now();
        ApiToken apiToken = apiTokenService.generate(tokenName, null, userId.longValue());
        return Map.of("token", apiToken.getValue());
    }

}

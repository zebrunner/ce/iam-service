package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.entity.Preference;
import com.zebrunner.iam.service.PreferenceService;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.request.v1.UpdatePreferenceRequest;
import com.zebrunner.iam.web.response.v1.PreferenceInfoResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserPreferenceController {

    private final Mapper mapper;
    private final PreferenceService preferenceService;

    @GetMapping("/{userId}/preferences")
    public List<PreferenceInfoResponse> get(@PathVariable("userId") int userId) {
        return mapper.map(preferenceService.getByUserId(userId), PreferenceInfoResponse.class);
    }

    @PutMapping("/{userId}/preferences")
    public List<PreferenceInfoResponse> save(@PathVariable("userId") int userId,
                                             @RequestBody List<UpdatePreferenceRequest> updatePreferenceRequests) {
        List<Preference> preferences = mapper.map(updatePreferenceRequests, Preference.class);
        preferences = preferenceService.saveAll(userId, preferences);
        return mapper.map(preferences, PreferenceInfoResponse.class);
    }

    @DeleteMapping("/{userId}/preferences")
    public List<PreferenceInfoResponse> resetToDefault(@PathVariable("userId") int userId) {
        List<Preference> preferences = preferenceService.resetToDefault(userId);
        return mapper.map(preferences, PreferenceInfoResponse.class);
    }

}

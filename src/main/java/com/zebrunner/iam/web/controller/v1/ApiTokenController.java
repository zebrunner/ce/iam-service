package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.HasPermission;
import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.iam.domain.entity.ApiToken;
import com.zebrunner.iam.service.ApiTokenService;
import com.zebrunner.iam.service.exception.BusinessConstraintError;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.request.v1.CreateApiTokenRequest;
import com.zebrunner.iam.web.response.v1.CreateApiTokenResponse;
import com.zebrunner.iam.web.response.v1.GetApiTokensResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/api-tokens", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiTokenController {

    private final Mapper mapper;
    private final ApiTokenService apiTokenService;

    @GetMapping
    public GetApiTokensResponse retrieveAllByUser(@RequestParam(required = false) Long userId,
                                                  @PrincipalId Integer invokerId,
                                                  @HasPermission("iam:api-tokens:read") boolean hasReadApiTokensPermission) {
        if (userId != null) {
            if (!Objects.equals(invokerId.longValue(), userId) && !hasReadApiTokensPermission) {
                throw new BusinessConstraintException(BusinessConstraintError.ACCESS_TO_USER_API_TOKENS_IS_NOT_PERMITTED, userId);
            }
        } else {
            // If userId is not provided, invoker's tokens set will be returned
            userId = invokerId.longValue();
        }
        List<ApiToken> apiTokens = apiTokenService.retrieveByUserId(userId);
        List<GetApiTokensResponse.Item> items = mapper.map(apiTokens, GetApiTokensResponse.Item.class);
        return new GetApiTokensResponse(items);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateApiTokenResponse create(@RequestBody @Valid CreateApiTokenRequest createApiTokenRequest,
                                         @PrincipalId Integer userId) {
        String name = createApiTokenRequest.getName();
        Instant expiresAt = createApiTokenRequest.getExpiresAt();
        ApiToken apiToken = apiTokenService.generate(name, expiresAt, userId.longValue());
        return mapper.map(apiToken, CreateApiTokenResponse.class);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void revoke(@PathVariable("id") @Positive Long id,
                       @PrincipalId Integer userId,
                       @HasPermission("iam:api-tokens:revoke") boolean hasRevokeApiTokensPermission) {
        ApiToken apiToken = apiTokenService.retrieveById(id);
        if (!Objects.equals(userId.longValue(), apiToken.getUserId()) && !hasRevokeApiTokensPermission) {
            throw new BusinessConstraintException(BusinessConstraintError.USER_API_TOKEN_REVOCATION_IS_NOT_PERMITTED);
        }
        apiTokenService.revoke(id);
    }

}

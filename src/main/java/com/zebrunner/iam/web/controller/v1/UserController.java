package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.HasPermission;
import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.domain.search.UserSearchCriteria;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import com.zebrunner.iam.web.documentation.v1.UserDocumentedController;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.patch.PatchOperation;
import com.zebrunner.iam.web.patch.PatchRequest;
import com.zebrunner.iam.web.patch.ValidEmailItem;
import com.zebrunner.iam.web.patch.ValidPatch;
import com.zebrunner.iam.web.request.v1.CreateInvitedUserRequest;
import com.zebrunner.iam.web.request.v1.CreateUserRequest;
import com.zebrunner.iam.web.request.v1.UpdateUserPasswordRequest;
import com.zebrunner.iam.web.response.v1.CreateUserResponse;
import com.zebrunner.iam.web.response.v1.FullUserInfoResponse;
import com.zebrunner.iam.web.response.v1.GetUsersResponse;
import com.zebrunner.iam.web.response.v1.UserInfoWithGroupsResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.List;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController implements UserDocumentedController {

    private final Mapper mapper;
    private final UserService userService;

    @Override
    @GetMapping
    @PreAuthorize("hasAnyPermission('iam:users:read', 'iam:users:update') or #isPublic")
    public SearchResult<UserInfoWithGroupsResponse> search(@RequestParam(name = "query", required = false) String query,
                                                           @RequestParam(name = "sortBy", required = false) String sortBy,
                                                           @RequestParam(name = "sortOrder", required = false) SortOrder sortOrder,
                                                           @RequestParam(name = "page", defaultValue = "1") Integer page,
                                                           @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                                           @RequestParam(name = "public", defaultValue = "true") boolean isPublic,
                                                           @RequestParam(name = "status", required = false) UserStatus status) {
        UserSearchCriteria searchCriteria = new UserSearchCriteria(
                query, sortBy, sortOrder, page, pageSize, isPublic, status
        );
        SearchResult<User> searchResult = userService.search(searchCriteria);
        return new SearchResult<>(
                searchResult, mapper.map(searchResult.getResults(), UserInfoWithGroupsResponse.class)
        );
    }

    @GetMapping(params = "ids")
    @PreAuthorize("hasPermission('iam:users:read')")
    public GetUsersResponse getUsersByIds(@RequestParam("ids") @Valid List<@Positive Integer> ids) {
        List<User> users = userService.getByIds(ids);
        List<GetUsersResponse.Item> items = mapper.map(users, GetUsersResponse.Item.class);

        return new GetUsersResponse(items);
    }

    @Override
    @GetMapping(params = "username")
    public UserInfoWithGroupsResponse getProfile(@RequestParam("username") @NotEmpty String username) {
        return userService.getWithPermissionsByUsername(username)
                          .map(user -> mapper.map(user, UserInfoWithGroupsResponse.class))
                          .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_USERNAME.withArgs(username));
    }

    @Override
    @GetMapping("/{id}")
    public Object getProfile(@PathVariable("id") @Positive Integer id,
                             @RequestParam(name = "_embedPreferences", defaultValue = "false") boolean embedPreferences) {
        User user = userService.getWithPermissionsById(id)
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(id));

        return embedPreferences
                ? mapper.map(user, FullUserInfoResponse.class)
                : mapper.map(user, UserInfoWithGroupsResponse.class);
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasPermission('iam:users:update')")
    public CreateUserResponse create(@RequestBody @Valid CreateUserRequest createUserRequest) {
        User user = mapper.map(createUserRequest, User.class);
        user = userService.create(user);
        return mapper.map(user, CreateUserResponse.class);
    }

    @Override
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(params = "invitation-token")
    public CreateUserResponse create(@RequestParam("invitation-token") @NotEmpty String invitationToken,
                                     @RequestBody @Valid CreateInvitedUserRequest createInvitedUserRequest) {
        User user = mapper.map(createInvitedUserRequest, User.class);
        user = userService.create(invitationToken, user);
        return mapper.map(user, CreateUserResponse.class);
    }

    @Override
    @PostMapping("/{id}/password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePassword(@PrincipalId Integer invokerId,
                               @PathVariable("id") @Positive Integer userId,
                               @RequestBody @Valid UpdateUserPasswordRequest updaterequestUserPassword,
                               @HasPermission("iam:users:update") boolean forceUpdate) {
        String oldPassword = updaterequestUserPassword.getOldPassword();
        String newPassword = updaterequestUserPassword.getNewPassword();

        if (forceUpdate && !invokerId.equals(userId)) {
            userService.forceUpdatePassword(userId, newPassword);
        } else {
            userService.updatePassword(userId, oldPassword, newPassword);
        }
    }

    @Override
    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void patch(@PathVariable("id") Integer id,
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/email")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/firstName")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/lastName")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/photoUrl")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/status")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/onboardingCompleted")
                      @Valid @RequestBody @ValidEmailItem PatchRequest patchRequest,
                      @HasPermission("iam:users:update") boolean fullUpdate) {
        User.UserBuilder userBuilder = User.builder()
                                           .id(id)
                                           .firstName(patchRequest.getReplaceValue("/firstName", String.class))
                                           .lastName(patchRequest.getReplaceValue("/lastName", String.class))
                                           .photoUrl(patchRequest.getReplaceValue("/photoUrl", String.class))
                                           .onboardingCompleted(patchRequest.getReplaceValue("/onboardingCompleted", boolean.class));

        if (fullUpdate) {
            userBuilder.email(patchRequest.getReplaceValue("/email", String.class))
                       .status(patchRequest.getReplaceValue("/status", UserStatus.class));
        }

        userService.patch(userBuilder.build());
    }

    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{userId}/groups/{groupId}")
    @PreAuthorize("hasPermission('iam:groups:update')")
    public void addUserToGroup(@PathVariable("userId") @Positive Integer userId,
                               @PathVariable("groupId") @Positive Integer groupId) {
        userService.addUserToGroup(userId, groupId);
    }

    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{userId}/groups/{groupId}")
    @PreAuthorize("hasPermission('iam:groups:update')")
    public void removeUserFromGroup(@PathVariable("userId") @Positive Integer userId,
                                    @PathVariable("groupId") @Positive Integer groupId) {
        userService.removeUserFromGroup(userId, groupId);
    }

    @Override
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('iam:users:delete')")
    public void delete(@PathVariable("id") Integer id) {
        userService.deleteById(id);
    }

}

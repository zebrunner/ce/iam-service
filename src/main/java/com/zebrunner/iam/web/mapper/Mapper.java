package com.zebrunner.iam.web.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface Mapper {

    <D, T> D map(T t, Class<D> dClass);

    default <D, T> List<D> map(Collection<T> collection, Class<D> clazz) {
        return collection.stream()
                         .map(o -> map(o, clazz))
                         .collect(Collectors.toList());
    }
}

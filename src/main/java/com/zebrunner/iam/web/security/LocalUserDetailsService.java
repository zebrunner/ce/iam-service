package com.zebrunner.iam.web.security;

import com.zebrunner.common.auth.user.AuthenticatedUser;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.exception.UnauthorizedAccessError;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class LocalUserDetailsService implements UserDetailsService {

    private final UserService userService;
    private final PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userService.getWithPermissionsByUsername(username)
                               .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " was not found"));
        if (user.getStatus() == UserStatus.INACTIVE) {
            throw UnauthorizedAccessError.USER_NOT_ACTIVE.withArgs(user.getId());
        }
        Set<String> permissions = permissionService.getSuperset(user.getGroups(), user.getPermissions());
        return AuthenticatedUser.builder()
                                .userId(user.getId())
                                .username(user.getUsername())
                                .password(user.getPassword())
                                .permissions(permissions)
                                .build();
    }

}

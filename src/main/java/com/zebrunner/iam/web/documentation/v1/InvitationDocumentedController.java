package com.zebrunner.iam.web.documentation.v1;

import com.zebrunner.common.auth.user.AuthenticatedUser;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.web.request.v1.SaveInvitationRequest;
import com.zebrunner.iam.web.response.v1.InvitationInfoResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.Explode;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import java.util.List;

public interface InvitationDocumentedController {

    @Operation(
            summary = "Retrieves a paginated set of invitations.",
            description = "Returns found invitations",
            tags = "Invitations",
            parameters = {
                    @Parameter(
                            name = "searchCriteria",
                            in = ParameterIn.QUERY,
                            explode = Explode.TRUE,
                            style = ParameterStyle.DEEPOBJECT,
                            schema = @Schema(implementation = SearchCriteria.class)
                    )
            },
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = SearchResult.class)
                            ),
                            description = "Returns found invitations"
                    )
            }
    )
    SearchResult<InvitationInfoResponse> search(String query, String sortBy, SortOrder sortOrder, Integer page, Integer pageSize);

    @Operation(
            summary = "Retrieves an invitation by the invitation token (pending only).",
            description = "A token is generated while creating an invitation.",
            tags = "Invitations",
            parameters = @Parameter(name = "invitation-token", in = ParameterIn.PATH, description = "The invitation token."),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns the found invitation.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = SaveInvitationRequest.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the invitation cannot be found.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = Object.class)
                            )
                    )
            }
    )
    InvitationInfoResponse getInvitation(String invitationToken);

    @Operation(
            summary = "Invites a batch of users to sign up.",
            description = "Invites a batch of users to sign up. Returns created invitations. If an invitation has already been submitted, it will be resend once again.",
            tags = "Invitations",
            responses = {
                    @ApiResponse(
                            responseCode = "202",
                            description = "Returns created invitations",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    array = @ArraySchema(schema = @Schema(implementation = SaveInvitationRequest.class))
                            )
                    )
            }
    )
    List<InvitationInfoResponse> inviteUsers(AuthenticatedUser authenticatedUser,
                                             List<SaveInvitationRequest> saveInvitationRequests);

    @Operation(
            summary = "Deletes an invitation by id.",
            description = "Deletes an invitation by id. The invited user cannot use the invitation link anymore.",
            tags = "Invitations",
            responses = {
                    @ApiResponse(responseCode = "204", description = "The invitation was deleted successfully."),
                    @ApiResponse(responseCode = "404", description = "The invitation was not found.")
            }
    )
    void deleteInvitation(Integer id);

}

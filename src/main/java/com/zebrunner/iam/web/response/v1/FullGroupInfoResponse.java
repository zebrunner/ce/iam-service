package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.domain.entity.User;
import lombok.Data;
import lombok.Value;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@JGlobalMap
public class FullGroupInfoResponse {

    private Integer id;
    private String name;
    @JsonProperty("isDefault")
    private Boolean isDefault;
    private Boolean invitable;
    private List<String> permissions;
    private List<UserInfo> users;

    @Value
    public static class UserInfo {

        Integer id;
        String username;
        String photoUrl;

    }

    @JMapConversion(from = {"permissions"}, to = {"permissions"})
    public List<String> convertPermissions(Set<Permission> permissions) {
        return permissions.stream()
                          .map(Permission::getName)
                          .collect(Collectors.toList());
    }

    @JMapConversion(from = {"users"}, to = {"users"})
    public List<UserInfo> convertUsers(List<User> users) {
        return users.stream()
                    .map(user -> new UserInfo(user.getId(), user.getUsername(), user.getPhotoUrl()))
                    .collect(Collectors.toList());
    }

}

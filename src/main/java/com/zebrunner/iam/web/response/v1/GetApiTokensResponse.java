package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.Instant;
import java.util.List;

@Value
public class GetApiTokensResponse {

    List<GetApiTokensResponse.Item> items;

    @Data
    @NoArgsConstructor
    @JGlobalMap
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Item {

        private Long id;
        private String name;
        private Instant expiresAt;

    }

}

package com.zebrunner.iam.web.response.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
public class GetUsersResponse {

    List<Item> items;

    @Data
    @NoArgsConstructor
    @JGlobalMap
    public static class Item {

        private Integer id;
        private String username;
        private String email;
        private String firstName;
        private String lastName;
        private String photoUrl;

    }

}

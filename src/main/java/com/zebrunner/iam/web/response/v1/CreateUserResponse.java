package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.iam.domain.entity.UserSource;
import lombok.*;

@Data
@JGlobalMap
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateUserResponse {

    private Integer id;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String photoUrl;
    private UserSource source;

}

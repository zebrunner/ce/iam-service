package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.domain.entity.Preference;
import com.zebrunner.iam.domain.entity.UserSource;
import com.zebrunner.iam.domain.entity.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FullUserInfoResponse {

    private Integer id;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String photoUrl;
    private Instant lastLogin;
    private Boolean onboardingCompleted;
    private UserStatus status;
    private UserSource source;
    private List<GroupInfo> groups;
    private List<String> permissions;
    private List<PreferenceInfo> preferences;
    private Instant registrationDateTime;

    @Value
    public static class GroupInfo {

        Integer id;
        String name;

    }

    @Value
    public static class PreferenceInfo {

        Integer id;
        String name;
        String value;

    }

    @JMapConversion(from = {"groups"}, to = {"groups"})
    public List<GroupInfo> convertGroups(Set<Group> groups) {
        return groups.stream()
                     .map(group -> new GroupInfo(group.getId(), group.getName()))
                     .collect(Collectors.toList());
    }

    @JMapConversion(from = {"permissions"}, to = {"permissions"})
    public List<String> convertPermissions(Set<Permission> permissions) {
        return permissions.stream()
                          .map(Permission::getName)
                          .collect(Collectors.toList());
    }

    @JMapConversion(from = {"preferences"}, to = {"preferences"})
    public List<PreferenceInfo> convertPreferences(Set<Preference> preferences) {
        return preferences.stream()
                          .map(preference -> new PreferenceInfo(preference.getId(), preference.getName(), preference.getValue()))
                          .collect(Collectors.toList());
    }

}

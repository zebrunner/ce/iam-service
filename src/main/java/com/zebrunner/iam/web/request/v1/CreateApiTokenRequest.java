package com.zebrunner.iam.web.request.v1;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.time.Instant;

@Data
public class CreateApiTokenRequest {

    @NotEmpty
    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant expiresAt;

}

package com.zebrunner.iam.persistance.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.persistance.GroupRefreshRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class GroupRefreshRepositoryImpl implements GroupRefreshRepository {

    private final EntityManager entityManager;

    @Override
    public void refresh(Group group) {
        if (group.getId() != null) {
            Group managedGroup = entityManager.find(group.getClass(), group.getId());
            entityManager.refresh(managedGroup);
        }
    }

}

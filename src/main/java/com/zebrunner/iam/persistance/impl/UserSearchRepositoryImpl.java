package com.zebrunner.iam.persistance.impl;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserSource;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.UserSearchCriteria;
import com.zebrunner.iam.persistance.UserSearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserSearchRepositoryImpl extends SimpleJpaRepository<User, Integer> implements UserSearchRepository {

    @Autowired
    public UserSearchRepositoryImpl(EntityManager em) {
        super(User.class, em);
    }

    @Override
    public SearchResult<User> findBySearchCriteria(UserSearchCriteria searchCriteria) {
        Sort sort = getSort(searchCriteria);
        Pageable pageable = PageRequest.of(searchCriteria.getPage() - 1, searchCriteria.getPageSize(), sort);

        Optional<Specification<User>> maybeUserSpecification = getSearchSpecification(searchCriteria);
        Page<User> searchResult = maybeUserSpecification.map(userExample -> findAll(userExample, pageable))
                                                        .orElseGet(() -> findAll(pageable));

        Specification<User> searchByIdsWithFetchSpecification = searchByIdsWithFetchSpecification(searchResult.getContent());
        List<User> usersWithGroupsAndPermissions = findAll(searchByIdsWithFetchSpecification);

        return new SearchResult<>(searchCriteria, searchResult.getTotalPages(), searchResult.getTotalElements(), usersWithGroupsAndPermissions);
    }

    private Sort getSort(SearchCriteria searchCriteria) {
        return searchCriteria.getSortBy() != null
                ? Sort.by(searchCriteria.getSortOrder().toDirection(), searchCriteria.getSortBy())
                : Sort.unsorted();
    }

    private Optional<Specification<User>> getSearchSpecification(UserSearchCriteria searchCriteria) {
        Optional<Example<User>> userExample = buildUserExample(searchCriteria);
        Optional<Example<User>> statusExample = buildStatusExample(searchCriteria.getStatus());

        return userExample.map(this::userSpecification)
                          .map(userSpecification -> statusExample.map(this::userSpecification)
                                                                 .map(userSpecification::and)
                                                                 .orElse(userSpecification))
                          .or(() -> statusExample.map(this::userSpecification));
    }

    private Specification<User> userSpecification(Example<User> example) {
        return (Specification<User>) (root, query, builder) -> QueryByExamplePredicateBuilder.getPredicate(root, builder, example);
    }

    private Specification<User> searchByIdsWithFetchSpecification(List<User> searchResult) {
        List<Integer> userIds = searchResult.stream()
                                            .map(User::getId)
                                            .collect(Collectors.toList());

        return (Specification<User>) (root, query, builder) -> {
            root.join("groups", JoinType.LEFT);
            root.join("permissions", JoinType.LEFT);
            query.distinct(true);
            Path<Integer> id = root.get("id");
            return id.in(userIds);
        };
    }

    private Optional<Example<User>> buildUserExample(UserSearchCriteria searchCriteria) {
        return Optional.ofNullable(searchCriteria.getQuery())
                       .map(query -> buildUserProbe(query, searchCriteria.isPublic()))
                       .map(user -> Example.of(user, getMatcher()));
    }

    private User buildUserProbe(String query, boolean isPublic) {
        User.UserBuilder userBuilder = User.builder()
                                           .username(query)
                                           .email(query);
        if (!isPublic) {
            userBuilder.lastName(query)
                       .firstName(query)
                       .status(UserStatus.fromString(query))
                       .source(UserSource.fromString(query));
        }
        return userBuilder.build();
    }

    private ExampleMatcher getMatcher() {
        return ExampleMatcher.matchingAny()
                             .withIgnoreCase()
                             .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
    }

    private Optional<Example<User>> buildStatusExample(UserStatus status) {
        return Optional.ofNullable(status)
                       .map($ -> Example.of(User.builder().status(status).build()));
    }

}

package com.zebrunner.iam.persistance.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.persistance.GroupSearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class GroupSearchRepositoryImpl extends SimpleJpaRepository<Group, Integer> implements GroupSearchRepository {

    @Autowired
    public GroupSearchRepositoryImpl(EntityManager em) {
        super(Group.class, em);
    }

    @Override
    public SearchResult<Group> findBySearchCriteria(SearchCriteria searchCriteria) {
        Sort sort = buildSort(searchCriteria);
        Pageable pageable = PageRequest.of(searchCriteria.getPage() - 1, searchCriteria.getPageSize(), sort);

        Optional<Specification<Group>> maybeSearchSpecification = getSearchSpecification(searchCriteria);
        Page<Group> searchResult = maybeSearchSpecification.map(groupExample -> findAll(groupExample, pageable))
                                                           .orElseGet(() -> findAll(pageable));

        Specification<Group> searchByIdsWithFetchSpecification = searchByIdsWithFetchSpecification(searchResult.getContent());
        List<Group> groupsWithUsersAndPermissions = findAll(searchByIdsWithFetchSpecification);

        return new SearchResult<>(searchCriteria, searchResult.getTotalPages(), searchResult.getTotalElements(), groupsWithUsersAndPermissions);
    }

    private Sort buildSort(SearchCriteria searchCriteria) {
        return searchCriteria.getSortBy() != null
                ? Sort.by(searchCriteria.getSortOrder().toDirection(), searchCriteria.getSortBy())
                : Sort.unsorted();
    }

    private Optional<Specification<Group>> getSearchSpecification(SearchCriteria searchCriteria) {
        Optional<Example<Group>> groupExample = buildGroupExample(searchCriteria);
        return groupExample.map(this::groupSpecification);
    }

    private Specification<Group> groupSpecification(Example<Group> example) {
        return (Specification<Group>) (root, query, builder) -> QueryByExamplePredicateBuilder.getPredicate(root, builder, example);
    }

    private Specification<Group> searchByIdsWithFetchSpecification(List<Group> searchResult) {
        List<Integer> groupIds = searchResult.stream()
                                             .map(Group::getId)
                                             .collect(Collectors.toList());
        return (Specification<Group>) (root, query, builder) -> {
            root.join("permissions", JoinType.LEFT);
            root.join("users", JoinType.LEFT);
            query.distinct(true);
            Path<Integer> id = root.get("id");
            return id.in(groupIds);
        };
    }

    private Optional<Example<Group>> buildGroupExample(SearchCriteria searchCriteria) {
        if (searchCriteria.getQuery() != null || searchCriteria.isPublic()) {
            Group.GroupBuilder builder = Group.builder();
            if (searchCriteria.getQuery() != null) {
                builder.name(searchCriteria.getQuery());
            }
            if (searchCriteria.isPublic()) {
                builder.invitable(searchCriteria.isPublic());
            }

            // we can use matchingAll() here because we search only by name
            ExampleMatcher matcher = ExampleMatcher.matchingAll()
                                                   .withIgnoreCase()
                                                   .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
            return Optional.of(Example.of(builder.build(), matcher));
        } else {
            return Optional.empty();
        }
    }
}

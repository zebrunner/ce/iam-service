package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.ApiToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface ApiTokenRepository extends JpaRepository<ApiToken, Long> {

    @Query(value = "SELECT * FROM api_tokens WHERE value = _system.crypt(:value, value);", nativeQuery = true)
    Optional<ApiToken> findByValue(String value);

    List<ApiToken> findAllByUserId(Long userId);

    @Modifying
    @Query("UPDATE ApiToken at SET at.lastUsedAt = :lastUsed WHERE at.id = :id")
    void updateLastUsed(@Param("id") Long id, @Param("lastUsed") Instant lastUsed);

    boolean existsByUserIdAndName(Long userId, String name);

    void deleteAllByUserId(Long userId);

}

package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupSearchRepository {

    SearchResult<Group> findBySearchCriteria(SearchCriteria searchCriteria);

}

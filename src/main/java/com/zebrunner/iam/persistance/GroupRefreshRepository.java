package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Group;

public interface GroupRefreshRepository {

    void refresh(Group group);

}

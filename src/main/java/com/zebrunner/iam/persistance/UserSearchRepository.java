package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.UserSearchCriteria;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSearchRepository {

    SearchResult<User> findBySearchCriteria(UserSearchCriteria searchCriteria);

}

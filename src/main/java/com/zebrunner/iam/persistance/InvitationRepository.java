package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Invitation;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation, Integer> {

    Optional<Invitation> findByEmail(String email);

    Optional<Invitation> findByToken(String token);

    @Query("select count(u) = 0 from User u where u.email = :email")
    boolean isEmailAvailable(@Param("email") String email);

    @NonNull
    @EntityGraph(value = "invitation-with-invitor-and-group", type = EntityGraph.EntityGraphType.LOAD)
    <S extends Invitation> Page<S> findAll(@NonNull Example<S> example, @NonNull Pageable pageable);

    @NonNull
    @EntityGraph(value = "invitation-with-invitor-and-group", type = EntityGraph.EntityGraphType.LOAD)
    Page<Invitation> findAll(@NonNull Pageable pageable);

}

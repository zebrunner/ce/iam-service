package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Preference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PreferenceRepository extends JpaRepository<Preference, Integer> {

    List<Preference> findByUserId(Integer userId);

    List<Preference> findByUserUsername(String username);

}

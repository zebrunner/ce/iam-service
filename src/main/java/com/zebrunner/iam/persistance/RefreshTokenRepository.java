package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    @Query(value = "SELECT * FROM refresh_tokens WHERE value = _system.crypt(:value, value);", nativeQuery = true)
    Optional<RefreshToken> findByValue(String value);

    void deleteAllByUserId(Long userId);

    void deleteByExpiresAtBefore(Instant expirationDate);

}
